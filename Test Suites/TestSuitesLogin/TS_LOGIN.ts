<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>TS_LOGIN</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>75886c0f-c4cc-4329-9161-2602177c1fe7</testSuiteGuid>
   <testCaseLink>
      <guid>365f4375-fdc6-4ba4-9c77-86350d8ff839</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/LoginFeature/TC_LOGIN_VAR_5</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>c2311a51-76c2-4082-ad9d-39987cdc0934</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/FileDataLogin/FD_LOGIN</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>c2311a51-76c2-4082-ad9d-39987cdc0934</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>username</value>
         <variableId>a8b06a39-c02b-48de-8029-b3c04beef8ef</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>c2311a51-76c2-4082-ad9d-39987cdc0934</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>password</value>
         <variableId>ac787be7-0ef5-46e2-a5ed-5c82dd532bb2</variableId>
      </variableLink>
   </testCaseLink>
</TestSuiteEntity>
