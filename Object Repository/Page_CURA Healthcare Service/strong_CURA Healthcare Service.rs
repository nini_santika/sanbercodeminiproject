<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_CURA Healthcare Service</name>
   <tag></tag>
   <elementGuidId>947f8826-0c89-4d2d-a151-a3c2d3e984ef</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Book Appointment'])[1]/following::strong[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>4a603ede-8bb0-4642-9d63-1881b05b36bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CURA Healthcare Service</value>
      <webElementGuid>ec1e6318-f40b-4ed9-86ce-6f34c0e31ac4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/footer[1]/div[@class=&quot;container&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-10 col-lg-offset-1 text-center&quot;]/h4[1]/strong[1]</value>
      <webElementGuid>62e50895-5b84-46e1-9ec7-9aa0b80addc9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Book Appointment'])[1]/following::strong[1]</value>
      <webElementGuid>ce365981-f8bf-43f2-bb90-879fd3882ce1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Comment'])[1]/following::strong[1]</value>
      <webElementGuid>144dd279-c9e1-45a6-a725-e121075b1b04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='(678) 813-1KMS'])[1]/preceding::strong[1]</value>
      <webElementGuid>806886ad-01cd-487f-8dc7-515ca3dd2143</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='info@katalon.com'])[1]/preceding::strong[1]</value>
      <webElementGuid>bdad924c-fe88-49eb-b819-84d78759c79d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//strong</value>
      <webElementGuid>36eb80a6-6d4a-4da6-88f5-7471117fa2ba</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'CURA Healthcare Service' or . = 'CURA Healthcare Service')]</value>
      <webElementGuid>4f582fb6-70fa-4da3-a7b8-4037a6a89b1d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
